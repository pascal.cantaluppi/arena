## Overview

This is a simple demonstration of how to run Arena and connect it to [Bull Queue](https://github.com/OptimalBits/bull).

## Requirements

- Firewall open for port 4735 and 4736
- No other services running on ports 4735 or 47360

## Install

`npm i`
`cd example`
`npm i`
`npm i pm2@latest -g`


## Running

<!-- `npm run start:bull` -->
`pm2 start example/arena.js`

Then open http://localhost:4735 in your browser.
